<?php $i = 1; ?>
<?= form_open('User/saveTransaction'); ?>
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Obat</th>
      <th scope="col">Jenis Barang</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($this->cart->contents())) : ?>
    <?php foreach ($this->cart->contents() as $items) :?>
      <input type="hidden" name="id" id="id">
      <tr>
        <td width="16%;"><?= $i; ?></td>
        <td><input type="hidden" name="name" value="<?= $items['name'] ?>"><?= $items['name'] ?></td>
        <td><input type="hidden" name="jenis_obat" value="<?= $items['jenis_obat'] ?>"><?= $items['jenis_obat'] ?></td>
        <td><a href="<?= site_url('User/deleteItem/' . $items['rowid']) ?>" class="btn btn-xs btn-danger">X</a></td>
      </tr>
    <?php $i++; ?>
    <?php endforeach; ?>
    <tr>
      <td>
        <button type="submit" class="btn btn-primary">Bayar</button>
      </td>
      <td colspan="4"></td>
    </tr>
    <?php else : ?>
      <tr>
        <td style="text-align: center;" colspan="5">Tidak ada barang yang di masukkan ke dalam shopping cart</td>
      </tr>
    <?php endif; ?>
    </tbody>
  </table>