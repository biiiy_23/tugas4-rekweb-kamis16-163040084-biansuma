<div class="container"><br>
    <?php if( $this->session->flashdata('status') ) : ?>
        <div class="alert alert-success" role="alert">
            <?= $this->session->flashdata('status'); ?>   
        </div>

    <?php elseif( $this->session->flashdata('status_salah') ) : ?>
        <div class="alert alert-danger" role="alert">
            <?= $this->session->flashdata('status_salah'); ?>   
        </div>
    <?php endif; ?>

        <div class="row" id="daftar-menu">

          <?php if (!empty($obat)) : ?>
          <?php foreach ($obat as $row) : ?>
             <div class="col-sm-3">
               <div class="card">
                <div class="card-body">
                  <input type="hidden" value="<?= $row->id_obat ?>">
                  <h4 class="card-title"><?= $row->nama_obat; ?></h4>
                  <p class="card-text">Ini memang obat</p>
                  <h4 class="card-title">Stok :<?= $row->stok ?></h4>
                  <h3 class="card-title">Rp. <?= number_format($row->harga,0,',','.') ?></h3>
                  <a href="<?= site_url('User/setToCart/' . $row->id_obat) ?>" class="btn btn-primary">Pesan Sekarang!</a>
                </div>
               </div>
            </div>
          <?php endforeach; ?>
          <?php else : ?>
            <div class="col-sm-3">
               <div class="card">
                <div class="card-body">
                  <h3>Data tidak ditemukan</h3>
                </div>
               </div>
            </div>
          <?php endif; ?>
        </div>

    </div>