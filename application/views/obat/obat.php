<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" id="formTambah" method="post">
					<div class="form-group">
						<label for="id_obat">Id Obat</label>
						<input type="number" class="form-control" id="id_obat" aria-describedby="emailHelp"
							   name="id_obat" placeholder="Masukan Id Obat">
					</div>
					<div class="form-group">
						<label for="nama_obat">Nama Obat</label>
						<input type="text" class="form-control" name="nama_obat" id="nama_obat"
							   placeholder="Masukan Nama Obat">
					</div>
					<div class="form-group">
						<label for="jenis_obat">Jenis Obat</label>
						<input type="text" class="form-control" id="jenis_obat" aria-describedby="emailHelp"
							   name="jenis_obat" placeholder="Masukan Jenis Obat">
					</div>
					<div class="form-group">
						<label for="stok">Stok Obat</label>
						<input type="number" class="form-control" name="stok" id="stok"
							   placeholder="Masukan Stok Obat">
					</div>
					<div class="form-group">
						<label for="harga">Harga</label>
						<input type="number" class="form-control" name="harga" id="harga"
							   placeholder="Masukan Harga Obat">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" id="submitAdd" class="btn btn-primary" name="add">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel1">Update Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" id="formUpdate" method="post">
					<div class="form-group">
						<label for="IdObatUpdate">ID Obat</label>
						<input type="number" class="form-control" id="idObatUpdate" aria-describedby="emailHelp"
							   name="idObatUpdate" placeholder="Masukan ID Obat">
					</div>
					<div class="form-group">
						<label for="namaObatUpdate">Nama Obat</label>
						<input type="text" value="" class="form-control" name="namaObatUpdate" id="namaObatUpdate"
							   placeholder="Masukan Nama Obat">
					</div>
					<div class="form-group">
						<label for="jenisObatUpdate">Jenis Obat</label>
						<input type="text" value="" class="form-control" id="jenisObatUpdate"
							   aria-describedby="emailHelp" name="jenisObatUpdate" placeholder="Masukan Jenis Obat">
					</div>
					<div class="form-group">
						<label for="stokUpdate">Stok Obat</label>
						<input type="number" value="" class="form-control" name="stokUpdate" id="stokUpdate"
							   placeholder="Masukan Stok Obat">
					</div>
					<div class="form-group">
						<label for="hargaUpdate">Harga Obat</label>
						<input type="number" value="" class="form-control" name="hargaUpdate" id="hargaUpdate"
							   placeholder="Masukan Harga Obat">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="add" id="submitUpdate">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Obat</h3>
			<button class="btn btn-success btn-sm" id="buttonAdd" data-toggle="modal" data-target="#exampleModal">
				Tambah
			</button>
			<div class="col-3" style="float: right; position: relative; left: 15px">
				<form action="<?= site_url('/obat/search/') ?>" method="get">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="keyword" class="form-control" placeholder="Search...">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary"
										style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- ketikan source yang ada di modul -->
			<table class="table table-striped table-bordered" id="myTable" style="margin-top: 20px;">
				<thead class="thead-dark">
					<tr>
						<th scope="col" style="text-align: center;">No</th>
						<th scope="col" style="text-align: center;">Nama Obat</th>
						<th scope="col" style="text-align: center;">Jenis Obat</th>
						<th scope="col" style="text-align: center;">Stok</th>
						<th scope="col" style="text-align: center;">Harga</th>
						<th scope="col" style="text-align: center;">Action</th>
					</tr>
				</thead>
				<?php if (empty($obt)) : ?>
					<tr>
						<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak ditemukan</td>
					</tr>
				<?php else : ?>
					<?php $no = 1; ?>
					<?php foreach ($obt as $key) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $key->nama_obat ?></td>
							<td><?= $key->jenis_obat ?></td>
							<td><?= $key->stok ?></td>
							<td><?= $key->harga ?></td>
							<td style="text-align: center;">
								<a href="<?= site_url('obat/detail/' . $key->id_obat) ?>" class="btn btn-sm btn-primary">Detail</a> | 
								<a href="" class="btn btn-sm btn-success update" id="<?= $key->id_obat ?>">Update</a> | 
								<a href="" class="btn btn-sm btn-danger delete" id="<?= $key->id_obat ?>">Delete</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url : "<?= site_url('obat/create') ?>",
				type : "POST",
				data: $("#formTambah").serialize(),
				dataType : "JSON",
				success: function() {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeout(function() {
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});

		$(".update").click(function (e) {
			e.preventDefault();
			$("#modalUpdate").modal('show');
			var id = $(this).attr("id");
			$.ajax({
				url: "<?= site_url('obat/edit') ?>",
				type: "POST",
				data: "id_obat=" + id,
				dataType: "JSON",
				success: function (data) {
					if (data.status == 200) {
						$("#idObatUpdate").val(data.obt[0].id_obat);
						$("#namaObatUpdate").val(data.obt[0].nama_obat);
						$("#jenisObatUpdate").val(data.obt[0].jenis_obat);
						$("#stokUpdate").val(data.obt[0].stok);
						$("#hargaUpdate").val(data.obt[0].harga);
					}
				},
				error: function (xhr, status, error) {
					alert(status + " : " + error);
				}
			})
		});

		$("#submitUpdate").click(function (e) {
			e.preventDefault();
			$.ajax({
				url : "<?= site_url('obat/update') ?>",
				type : "POST",
				data: $("#formUpdate").serialize(),
				dataType : "JSON",
				success: function() {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil diubah",
						icon: "success",
						buttons: false,
					});
					setTimeout(function() {
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});

		$(".delete").click(function (e) {
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakah anda yakin ingin menghapus?",
				text: "Data yang terhapus tidak bisa dipulihkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: "<?= site_url('obat/delete') ?>",
						type: "POST",
						data: "id_obat=" + id,
						dataType: "JSON",
						success: function(data) {
							if (data.status == true) {
								swal({
									title: "Success",
									text: "Data berhasil dihapus",
									icon: "success",
									buttons: false,
								});
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						},
						error: function(xhr, status, error) {
							alert(status + " : " + error);
						}
					});
				} else {
					swal("Batal menghapus data");
				}
			});
		});
	});
</script>


