<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h4 class="card-title">Id Obat : <?= $obat[0]->id_obat ?></h4>
			<h5 class="card-title">Nama Obat : <?= $obat[0]->nama_obat ?></h5>
			<h6 class="card-subtitle mb-2 text-muted">Jenis Obat : <?= $obat[0]->jenis_obat ?></h6>
			<p class="card-text">Stok Barang : <?= $obat[0]->stok ?></p>
			<p class="card-text">Harga : Rp.<?= $obat[0]->harga ?></p>
			<a href="<?= site_url('obat') ?>" class="card-link">Kembali</a>
		</div>
	</div>
</div>
