<?php if (empty($detail)) : ?>
	<div class="container mt-5">
		<div class="card" style="width: 18rem;">
			<div class="card-body">
				<h4 class="card-title">Maaf data yang anda cari tidak ditemukan</h4>
			</div>
		</div>
	</div>

	<div class="container mt-5">
		<h3>Harga</h3>
	</div>
	<?php else : ?>
		<?php $total = 0; ?>
		<?php foreach ($detail as $key) : ?>
			<div class="container mt-5">
				<div class="card" style="width: 18rem;">
					<div class="card-body">
						<h4 class="card-title">Id Detail : <?= $key->id_detail ?></h4>
						<h5 class="card-title">Nama Obat : <?= $key->nama ?></h5>
						<h6 class="card-subtitle mb-2 text-muted">Jenis Obat : <?= $key->jenis ?></h6>
						<p class="card-text">Harga : Rp.<?= $key->harga ?></p>
					</div>
				</div>
			</div>
			<?php $total = $total + $key->harga ?>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="row m-5">
		<h3>Harga : Rp.<?= $total ?></h3>
	</div>
