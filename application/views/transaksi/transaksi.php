<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Obat</h3>
			</div>
			<!-- ketikan source yang ada di modul -->
			<table class="table table-striped table-bordered" id="myTable" style="margin-top: 20px;">
				<thead class="thead-dark">
					<tr>
						<th scope="col" style="text-align: center;">ID Transaksi</th>
						<th scope="col" style="text-align: center;">Nama Pembeli</th>
						<th scope="col" style="text-align: center;">Tanggal Transaksi</th>
						<th scope="col" style="text-align: center;">Aksi</th>
					</tr>
				</thead>
				<?php if (empty($tr)) : ?>
					<tr>
						<td colspan="4" style="text-align: center;">Maaf data yang anda cari tidak ditemukan</td>
					</tr>
				<?php else : ?>
					<?php foreach ($tr as $key) : ?>
						<tr>
							<td><?= $key->id_transaksi ?></td>
							<td><?= $key->user ?></td>
							<td><?= $key->tanggal_transaksi ?></td>
							<td style="text-align: center;">
								<a href="<?= site_url('Detail/index/' . $key->id_transaksi) ?>" class="btn btn-sm btn-primary">Detail</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url : "<?= site_url('obat/create') ?>",
				type : "POST",
				data: $("#formTambah").serialize(),
				dataType : "JSON",
				success: function() {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeout(function() {
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});

		$(".update").click(function (e) {
			e.preventDefault();
			$("#modalUpdate").modal('show');
			var id = $(this).attr("id");
			$.ajax({
				url: "<?= site_url('obat/edit') ?>",
				type: "POST",
				data: "id_obat=" + id,
				dataType: "JSON",
				success: function (data) {
					if (data.status == 200) {
						$("#idObatUpdate").val(data.obt[0].id_obat);
						$("#namaObatUpdate").val(data.obt[0].nama_obat);
						$("#jenisObatUpdate").val(data.obt[0].jenis_obat);
						$("#stokUpdate").val(data.obt[0].stok);
						$("#hargaUpdate").val(data.obt[0].harga);
					}
				},
				error: function (xhr, status, error) {
					alert(status + " : " + error);
				}
			})
		});

		$("#submitUpdate").click(function (e) {
			e.preventDefault();
			$.ajax({
				url : "<?= site_url('obat/update') ?>",
				type : "POST",
				data: $("#formUpdate").serialize(),
				dataType : "JSON",
				success: function() {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil diubah",
						icon: "success",
						buttons: false,
					});
					setTimeout(function() {
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});

		$(".delete").click(function (e) {
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakah anda yakin ingin menghapus?",
				text: "Data yang terhapus tidak bisa dipulihkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: "<?= site_url('obat/delete') ?>",
						type: "POST",
						data: "id_obat=" + id,
						dataType: "JSON",
						success: function(data) {
							if (data.status == true) {
								swal({
									title: "Success",
									text: "Data berhasil dihapus",
									icon: "success",
									buttons: false,
								});
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						},
						error: function(xhr, status, error) {
							alert(status + " : " + error);
						}
					});
				} else {
					swal("Batal menghapus data");
				}
			});
		});
	});
</script>


