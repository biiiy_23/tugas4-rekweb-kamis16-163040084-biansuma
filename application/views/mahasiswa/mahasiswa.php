<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Mahasiswa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" id="formTambah" method="post">
					<div class="form-group">
						<label for="nrp">NRP</label>
						<input type="number" class="form-control" id="nrp" aria-describedby="emailHelp"
							   name="nrp" placeholder="Masukan NRP">
					</div>
					<div class="form-group">
						<label for="nama">Nama Mahasiswa</label>
						<input type="text" class="form-control" name="nama" id="nama"
							   placeholder="Masukan Nama Mahasiswa">
					</div>
					<div class="form-group">
						<label for="email">Email address</label>
						<input type="email" class="form-control" id="email" aria-describedby="emailHelp"
							   name="email" placeholder="Masukan Email">
					</div>
					<div class="form-group">
						<label for="jurusan">Jurusan</label>
						<input type="text" class="form-control" name="jurusan" id="jurusan"
							   placeholder="Masukan Jurusan">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" id="submitAdd" class="btn btn-primary" name="add">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel1">Update Mahasiswa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" id="formUpdate" method="post">
					<input type="hidden" name="id" id="id_mhs">
					<div class="form-group">
						<label for="nrpUpdate">NRP</label>
						<input type="number" class="form-control" id="nrpUpdate" aria-describedby="emailHelp"
							   name="nrpUpdate" placeholder="Masukan NRP">
					</div>
					<div class="form-group">
						<label for="namaUpdate">Nama Mahasiswa</label>
						<input type="text" value="" class="form-control" name="namaUpdate" id="namaUpdate"
							   placeholder="Masukan Nama Mahasiswa">
					</div>
					<div class="form-group">
						<label for="emailUpdate">Email address</label>
						<input type="email" value="" class="form-control" id="emailUpdate"
							   aria-describedby="emailHelp" name="emailUpdate" placeholder="Masukan Email">
					</div>
					<div class="form-group">
						<label for="jurusanUpdate">Jurusan</label>
						<input type="text" value="" class="form-control" name="jurusanUpdate" id="jurusanUpdate"
							   placeholder="Masukan Jurusan">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="add" id="submitUpdate">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Mahasiswa</h3>
			<button class="btn btn-success btn-sm" id="buttonAdd" data-toggle="modal" data-target="#exampleModal">
				Tambah
			</button>
			<div class="col-3" style="float: right; position: relative; left: 15px">
				<form action="<?= site_url('/mahasiswa/search/') ?>" method="get">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="keyword" class="form-control" placeholder="Search...">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary"
										style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- ketikan source yang ada di modul -->
			<table class="table table-striped table-bordered" id="myTable" style="margin-top: 20px;">
				<thead class="thead-dark">
					<tr>
						<th scope="col" style="text-align: center;">No</th>
						<th scope="col" style="text-align: center;">Nama</th>
						<th scope="col" style="text-align: center;">NRP</th>
						<th scope="col" style="text-align: center;">Email</th>
						<th scope="col" style="text-align: center;">Jurusan</th>
						<th scope="col" style="text-align: center;">Action</th>
					</tr>
				</thead>
				<?php if (empty($mhs)) : ?>
					<tr>
						<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak ditemukan</td>
					</tr>
				<?php else : ?>
					<?php $no = 1; ?>
					<?php foreach ($mhs as $key) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $key->nama ?></td>
							<td><?= $key->nrp ?></td>
							<td><?= $key->email ?></td>
							<td><?= $key->jurusan ?></td>
							<td style="text-align: center;">
								<a href="<?= site_url('mahasiswa/detail/' . $key->id) ?>" class="btn btn-sm btn-primary">Detail</a> | 
								<a href="" class="btn btn-sm btn-success update" id="<?= $key->id ?>">Update</a> | 
								<a href="" class="btn btn-sm btn-danger delete" id="<?= $key->id ?>">Delete</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url : "<?= site_url('mahasiswa/create') ?>",
				type : "POST",
				data: $("#formTambah").serialize(),
				dataType : "JSON",
				success: function() {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false
					});
					setTimeout(function() {
						location.reload();
					}, 2000);
				},
				error: function(xhr, status, error) {
					alert(status + " : " + error);
				}
			});
		});

		$(".update").click(function (e) {
			// ketikan source yang ada di modul
		});

		$("#submitUpdate").click(function (e) {
			// ketikan source yang ada di modul
		});

		$(".delete").click(function (e) {
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakah anda yakin ingin menghapus?",
				text: "Data yang terhapus tidak bisa dipulihkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: "<?= site_url('mahasiswa/delete') ?>",
						type: "POST",
						data: "id=" + id,
						dataType: "JSON",
						success: function(data) {
							if (data.status == true) {
								swal({
									title: "Success",
									text: "Data berhasil dihapus",
									icon: "	success",
									buttons: false
								});
								setTimeout(function() {
								location.reload();
								}, 2000);
							}
						},
						error: function(xhr, status, error) {
							alert(status + " : " + error);
						}
					});
				} else {
					swal("Batal menghapus data");
				}
			});
		});
	});
</script>


