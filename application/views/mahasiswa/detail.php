<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $mahasiswa[0]->nama ?></h5>
			<h6 class="card-subtitle mb-2 text-muted"><?= $mahasiswa[0]->nrp ?></h6>
			<p class="card-text"><?= $mahasiswa[0]->email ?></p>
			<p class="card-text"><?= $mahasiswa[0]->jurusan ?></p>
			<a href="<?= site_url('mahasiswa') ?>" class="card-link">Kembali</a>
		</div>
	</div>
</div>
