<!doctype html>
<html lang="en">
  <head>
    <title>Obat Rekweb | Berbagai macam obat untuk mengobati REKWEB</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/lib/customerStyle.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
      body {
        background-image: url(<?= base_url('assets/img/bg4.jpg')?>);
      }
    </style>

  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url('CustomerPage') ?>">
          <img src="<?= base_url('assets/img/logoobatrekweb.png') ?>" height="40" class="d-inline-block align-top" alt="">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('User/CustomerPage') ?>">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('User/cartPage') ?>"><i class="fa fa-shopping-cart"> Cart :<?= count($this->cart->contents()); ?> Items</i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('User/logout') ?>">Logout</a>
            </li>
          </ul>
        </div>
        <form action="<?= base_url('User/search')?>" method="GET" class="search-form form-inline my-2 my-lg-0">
          <input class="form-control" type="text" placeholder="Search ..." aria-label="Search" name="keyword">
          <button class="search-close btn btn-outline-success" type="submit">Search<i class="fa fa-close"></i></button>
        </form>
      </div>
    </nav>

    