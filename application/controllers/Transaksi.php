<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040084-biansuma";
	}

	public function index()
	{
		$data['judul'] = 'Penjualan Obat';
		$data['transaksi'] = json_decode($this->curl->simple_get($this->API . '/Transaksi/'));
		$data['tr'] = $data['transaksi'];
		$data['content'] = 'transaksi/transaksi';

		$this->load->view('template/template', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Penjualan Obat';
		$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Transaksi/', ["id_transaksi" => $id]));
		$data['content'] = 'transaksi/detail';

		$this->load->view('template/template', $data);
	}
}