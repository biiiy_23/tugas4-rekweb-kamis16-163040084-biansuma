<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040084-biansuma";
	}

	public function index($id)
	{
		$data['judul'] = 'Penjualan Obat';
		$data['detail'] = json_decode($this->curl->simple_get($this->API . '/Detail/', ["id_transaksi" => $id]));
		$data['content'] = 'transaksi/detail';

		$this->load->view('template/template', $data);
	}
}