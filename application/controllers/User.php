<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040084-biansuma";
	}

	public function index()
	{
		if ($this->session->has_userdata('username')) {
			if ($this->session->userdata('username') == 'Admin') {
				redirect('Obat');
			} else {
				redirect('User/CustomerPage');
			}
		} else {
			$this->load->view('Login');
		}
	}

	public function cekLogin() {
		$this->_userRules();

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('status', 'Masukkan username dan password!');
			redirect('User');
		} else {
			$user = [
				'username' => $username,
				'password' => $password
			];

			$status = json_decode($this->curl->simple_post($this->API . '/User/', $user, array(CURLOPT_BUFFERSIZE => 10)), true);

			if ($status) {
				if ($status['user_level'] == '1') {
					$this->session->set_userdata('username', $status['username']);
					redirect('Obat');
				} else {
					$this->session->set_userdata('username', $status['username']);
					$this->session->set_userdata('id_user', $status['id_user']);
					redirect('User/CustomerPage');
				}			
			} else {
				$this->session->set_flashdata('status', 'Username atau password salah!');
				redirect();
			}
		}
	}

	public function _userRules() {
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect();
	}

	public function CustomerPage() {
		if ($this->session->has_userdata('username')) {
			if ($this->session->userdata['username'] != 'Admin') {
				$data['judul'] = 'Penjualan Obat';
				$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/'));
				$data['obt'] = $data['obat'];
				$data['content'] = 'customer/customer';

				$this->load->view('template/templateCustomer', $data);
			} else {
				redirect('Obat');
			}
			
		} else {
			redirect();
		}
	}

	public function cartPage() {
		$data['content'] = 'customer/order';

		$this->load->view('template/templateCustomer', $data);
	}

	public function setToCart($id) {
		$data = json_decode($this->curl->simple_get($this->API . '/Obat/', ["id_obat" => $id]), true);
		$keranjang = [
			'id' => $data[0]['id_obat'],
			'name' => $data[0]['nama_obat'],
			'qty' => $data[0]['stok'],
			'price' => $data[0]['harga'],
			'jenis_obat' => $data[0]['jenis_obat']
		];
		if ($keranjang['qty'] == 0) {
			$this->session->set_flashdata('status_salah', 'Stok Habis!');
			redirect('User/CustomerPage');
		} else {
			$cart = $keranjang;
		$status = $this->cart->insert($cart);
		redirect('User/cartPage');
		}
	}

	public function deleteItem($rowid) {
		$this->cart->update(['rowid' => $rowid, 'qty' => 0]);
		redirect('User/cartPage');
	}

	public function search() {
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Penjualan Obat';
		$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/search/?keyword=' . urldecode($search)));
		$data['obt'] = $data['obat'];
		$data['content'] = 'customer/customer';

		$this->load->view('template/templateCustomer', $data);
	}

	public function saveTransaction() {
		$data = [
			'tanggal_transaksi' => date("Y-m-d"),
			'id_user' => $this->session->userdata('id_user')
		];

		$transaksi = $this->curl->simple_post($this->API . '/Transaksi/', $data, array(CURLOPT_BUFFERSIZE => 10));

		var_dump($transaksi);

		$transaksi_id = $this->db->insert_id();

		foreach ($this->cart->contents() as $items) {
			$detail = [
				'id_transaksi' => $transaksi_id,
				'id_obat' => $items['id']
			];
			$this->curl->simple_post($this->API . '/Detail/', $data, array(CURLOPT_BUFFERSIZE => 10));

		$this->cart->destroy();
		redirect('User/CustomerPage');
	}
}
}