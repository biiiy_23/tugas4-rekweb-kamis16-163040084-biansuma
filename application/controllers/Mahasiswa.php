<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rest_server";
	}

	public function index()
	{
		$data['judul'] = 'Latihan Rest API';
		$data['mahasiswa'] = json_decode($this->curl->simple_get($this->API . '/mahasiswa/'));
		$data['mhs'] = $data['mahasiswa'];
		$data['content'] = 'mahasiswa/mahasiswa';

		$this->load->view('template/template', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Latihan Rest API';
		$data['mahasiswa'] = json_decode($this->curl->simple_get($this->API . '/mahasiswa/', ["id" => $id]));
		$data['content'] = 'mahasiswa/detail';

		$this->load->view('template/template', $data);
	}

	public function search()
	{
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Latihan Rest API';
		$data['mahasiswa'] = json_decode($this->curl->simple_get($this->API . '/mahasiswa/search/?cari=' . urldecode($search)));
		$data['mhs'] = $data['mahasiswa'];
		$data['content'] = 'mahasiswa/mahasiswa';

		$this->load->view('template/template', $data);
	}

	public function create()
	{
		$nama = $this->input->post('nama');
		$nrp = $this->input->post('nrp');
		$email = $this->input->post('email');
		$jurusan = $this->input->post('jurusan');

		$data = [
			'nama' => $nama,
			'nrp' => $nrp,
			'email' => $email,
			'jurusan' => $jurusan,
		];

		$this->curl->simple_post($this->API . '/mahasiswa/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(["status" => true]);
	}

	public function edit()
	{
		$id = $this->input->post("id");
		$params = ['id' => $id];
		$data['mahasiswa'] = json_decode($this->curl->simple_get($this->API . '/mahasiswa', $params));
		$mahasiswa = $data['mahasiswa'];
		$json = json_encode(["status" => 200, "mhs" => $mahasiswa]);
		echo $json;
	}

	public function update()
	{
		$nama = $this->input->post('nama');
		$nrp = $this->input->post('nrp');
		$email = $this->input->post('email');
		$jurusan = $this->input->post('jurusan');
		$id = $this->input->post('id');

		$data = [
			'id' => $id,
			'nama' => $nama,
			'nrp' => $nrp,
			'email' => $email,
			'jurusan' => $jurusan,
		];

		$this->curl->simple_put($this->API . '/mahasiswa/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(["status" => true]);
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->curl->simple_delete($this->API . '/mahasiswa/', ['id' => $id], array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(["status" => true]);
	}
}
