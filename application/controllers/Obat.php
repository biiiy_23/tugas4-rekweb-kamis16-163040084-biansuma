<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040084-biansuma";
	}

	public function index()
	{
		if ($this->session->has_userdata('username')) {
			if ($this->session->userdata('username') == 'admin') {
				$data['judul'] = 'Penjualan Obat';
				$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/'));
				$data['obt'] = $data['obat'];
				$data['content'] = 'obat/obat';

				$this->load->view('template/template', $data);
			} else {
				redirect('User/CustomerPage');
			}
		} else {
			redirect();
		}
	}

	public function detail($id)
	{
		$data['judul'] = 'Penjualan Obat';
		$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/', ["id_obat" => $id]));
		$data['content'] = 'obat/detail';

		$this->load->view('template/template', $data);
	}

	public function search()
	{
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Penjualan Obat';
		$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/search/?keyword=' . urldecode($search)));
		$data['obt'] = $data['obat'];
		$data['content'] = 'obat/obat';

		$this->load->view('template/template', $data);
	}

	public function create()
	{
		$id = $this->input->post('id_obat');
		$nama = $this->input->post('nama_obat');
		$jenis = $this->input->post('jenis_obat');
		$stok = $this->input->post('stok');
		$harga = $this->input->post('harga');

		$data = [
			'id_obat' => $id,
			'nama_obat' => $nama,
			'jenis_obat' => $jenis,
			'stok' => $stok,
			'harga' => $harga
		];

		$this->curl->simple_post($this->API . '/Obat/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(["status" => true]);
	}

	public function edit()
	{
		$id = $this->input->post("id_obat");
		$params = ['id_obat' => $id];
		$data['obat'] = json_decode($this->curl->simple_get($this->API . '/Obat/', $params));
		$obat = $data['obat'];
		$json = json_encode(["status" => 200, "obt" => $obat]);
		echo $json;
	}

	public function update()
	{
		$id = $this->input->post('idObatUpdate');
		$nama = $this->input->post('namaObatUpdate');
		$jenis = $this->input->post('jenisObatUpdate');
		$stok = $this->input->post('stokUpdate');
		$harga = $this->input->post('hargaUpdate');

		$data = [
			'id_obat' => $id,
			'nama_obat' => $nama,
			'jenis_obat' => $jenis,
			'stok' => $stok,
			'harga' => $harga
		];

		$this->curl->simple_put($this->API . '/Obat/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(["status" => true]);
	}

	public function delete()
	{
		$id = $this->input->post('id_obat');
		json_decode($this->curl->simple_delete($this->API . '/Obat/', ['id_obat' => $id], array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(["status" => true]);
	}
}
